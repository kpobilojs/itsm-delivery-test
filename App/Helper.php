<?php

function dump($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

function dd($var)
{
    dump($var);
    die;
}

function ff($var)
{
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    die;
}

function summonTwoDateIntervals($interval1, $interval2)
{
    $e = new DateTime('00:00');
    $f = clone $e;
    $e->add($interval1);
    $e->add($interval2);

    return $f->diff($e);
}