<?php

namespace App\Controllers;

use App\Models\GroupMembers;
use App\Models\TimeLog;
use App\Models\User;
use Core\Controller;
use Core\View;
use DateTime;
use Exception;
use JsonException;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Home extends Controller
{
    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function indexAction(): void
    {
        $userId = $_SESSION['user_id'];
        $user = User::getById($userId);
        $user['time_log'] = TimeLog::getCurrentByUserId($userId);

        $group = GroupMembers::getGroupMemberByUserId($userId);
        foreach ($group as $key => $groupMember) {
            $group[$key]['time_log'] = TimeLog::getCurrentByUserId($groupMember['user_id']);
        }

        $hourReports = [];
        $currentMonth = '';
        if ($user['role'] === GroupMembers::ROLE_ADMIN) {
            $users = User::getAll();
            foreach ($users as $userForList) {
                $hourReports[$userForList['full_name']] = TimeLog::calculateHourReport($userForList['user_id']);
            }
        } elseif ($user['role'] === GroupMembers::ROLE_GROUP_LEADER) {
            foreach ($group as $groupMember) {
                $hourReports[$groupMember['full_name']] = TimeLog::calculateHourReport($groupMember['user_id']);
            }
        } else {
            $hourReport = TimeLog::calculateHourReport($user['user_id']);
            $currentMonth = array_key_first($hourReport);
            $hourReports[$user['full_name']] = $hourReport;
        }

        View::renderTemplate('home.twig', [
            'user' => $user,
            'group' => $group,
            'hourReports' => $hourReports,
            'currentMonth' => $currentMonth,
        ]);
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function loginFormAction(): void
    {
        View::renderTemplate('loginForm.twig');
    }

    /**
     * @throws JsonException
     */
    public function signInAction(): void
    {
        $username = $_POST['username'] ?? null;
        $password = $_POST['password'] ?? null;

        if (!$username) {
            $this->ajaxError('Empty login');
        }

        if (!$password) {
            $this->ajaxError('Empty password');
        }

        $user = User::getByCredentials($username, $password);

        if (empty($user)) {
            $this->ajaxError('Invalid credentials');
        }

        $_SESSION['login'] = 'OK';
        $_SESSION['user_id'] = $user['user_id'];

        $this->ajaxRedirect('/', 'Login successful');
    }

    /**
     */
    public function signOutAction(): void
    {
        $_SESSION = [];
        session_destroy();

        $this->redirect('/');
    }

    /**
     * @throws JsonException
     */
    public function startLoggingAction()
    {
        TimeLog::startLogging($_SESSION['user_id']);

        $this->ajaxRedirect('/', 'Started at: ' . (new DateTime())->format('Y-m-d H:i:s'));
    }

    /**
     * @throws JsonException
     */
    public function stopLoggingAction()
    {
        TimeLog::stopLogging($_SESSION['user_id']);

        $this->ajaxRedirect('/', 'Stopped at: ' . (new DateTime())->format('Y-m-d H:i:s'));
    }
}
