<?php

namespace App\Models;

use Core\Model;
use DateTime;
use PDO;

class GroupMembers extends Model
{
    public const ROLE_ADMIN = 1;
    public const ROLE_GROUP_LEADER = 2;

    public static function getGroupMemberByUserId(int $userId): array
    {
        $db = static::getDB();
        $stmt = $db->prepare("
            SELECT *
            FROM user
            WHERE user_id in (
                SELECT user_id
                FROM group_member
                WHERE group_id = (
                    SELECT group_id
                    FROM group_member
                    WHERE user_id = :userId
                )
            )
        ");
        $stmt->execute([
            'userId' => $userId,
        ]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
