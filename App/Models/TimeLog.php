<?php

namespace App\Models;

use Core\Model;
use DateTime;
use Exception;
use PDO;

class TimeLog extends Model
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_IDLE = 'idle';

    /**
     * @param $userId
     * @return array
     * @throws Exception
     */
    public static function calculateHourReport($userId): array
    {
        $result = [];
        $allUserLogs = self::getAllByUserId($userId);

        foreach ($allUserLogs as $log) {

            $startTime = new DateTime($log['started']);
            if ($log['finished'] !== null) {
                $endTime = new DateTime($log['finished']);
            } else {
                $endTime = new DateTime();
            }

            // every log time interval is either aggregated within one date or is split for two dates
            if ($startTime->format('Y-m-d') === $endTime->format('Y-m-d')) {
                $dateKey = $startTime->format('Y-m-d');
                if (!array_key_exists($dateKey, $result)) {
                    $result[$dateKey] = $startTime->diff($endTime);
                } else {
                    $result[$dateKey] = summonTwoDateIntervals($result[$dateKey], $startTime->diff($endTime));
                }
            } else {
                $firstDayKey = $startTime->format('Y-m-d');
                $secondDayKey = $endTime->format('Y-m-d');
                $firstDayEndTime = new DateTime($startTime->format('Y-m-d') . ' 23:59:59');
                $secondDayStartTime = new DateTime($endTime->format('Y-m-d') . '00:00:00');
                if (!array_key_exists($firstDayKey, $result)) {
                    $result[$firstDayKey] = $startTime->diff($firstDayEndTime);
                } else {
                    $result[$firstDayKey] = summonTwoDateIntervals(
                        $result[$firstDayKey],
                        $startTime->diff($firstDayEndTime)
                    );
                }
                if (!array_key_exists($secondDayKey, $result)) {
                    $result[$secondDayKey] = $secondDayStartTime->diff($endTime);
                } else {
                    $result[$secondDayKey] = summonTwoDateIntervals(
                        $result[$secondDayKey],
                        $secondDayStartTime->diff($endTime)
                    );
                }
            }
        }

        $data = [];
        foreach ($result as $keyDate => $interval) {
            $keyMonth = (new DateTime($keyDate))->format('F Y');
            $keyDay = (new DateTime($keyDate))->format('d');
            $data[$keyMonth]['days'][$keyDay] = $interval->format('%H:%I:%S');

            if (!array_key_exists('total', $data[$keyMonth])) {
                $data[$keyMonth]['total'] = $interval;
            } else {
                $data[$keyMonth]['total'] = summonTwoDateIntervals($data[$keyMonth]['total'], $interval);
            }
        }

        foreach ($data as $key => $month) {
            $data[$key]['total'] =
                round(
                    $data[$key]['total']->days * 24 +
                    $data[$key]['total']->h +
                    ($data[$key]['total']->i / 60),
                    2
                );
        }

        return $data;
    }

    public static function getCurrentByUserId(int $userId): array
    {
        $db = static::getDB();
        $stmt = $db->prepare("
            SELECT *
            FROM time_log
            WHERE user_id = :userId
            ORDER BY id DESC
            LIMIT 1;
        ");
        $stmt->execute([
            'userId' => $userId,
        ]);

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($result) && array_key_exists(0, $result)) {
            return $result[0];
        }

        return [];
    }

    public static function getAllByUserId(int $userId): array
    {
        $db = static::getDB();
        $stmt = $db->prepare("
            SELECT *
            FROM time_log
            WHERE user_id = :userId
            ORDER BY id DESC;
        ");
        $stmt->execute([
            'userId' => $userId,
        ]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $userId
     */
    public static function startLogging(int $userId): void
    {
        $db = static::getDB();
        $stmt = $db->prepare("
            INSERT INTO time_log
            (user_id, started, status)
            VALUES (:userId, :started, :status);
        ");
        $stmt->execute([
            'userId' => $userId,
            'started' => (new DateTime())->format('Y-m-d H:i:s'),
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @param int $userId
     */
    public static function stopLogging(int $userId): void
    {
        $db = static::getDB();

        $stmt = $db->prepare("
            SELECT MAX(id) as max_id
            FROM time_log
            WHERE user_id = :userId;
        ");
        $stmt->execute([
            'userId' => $userId,
        ]);
        $lastLogId = $stmt->fetchAll(PDO::FETCH_ASSOC)[0]['max_id'];

        $stmt = $db->prepare("
            UPDATE time_log
            SET finished = :finished,
                status = :status
            WHERE id = :lastLogId;
        ");
        $stmt->execute([
            'finished' => (new DateTime())->format('Y-m-d H:i:s'),
            'status' => self::STATUS_IDLE,
            'lastLogId' => $lastLogId,
        ]);
    }
}
