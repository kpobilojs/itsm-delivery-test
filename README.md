# Test task for ITSM Delivery

## Requirements
* Php 7.3
* Mysql or MariaDB, one of the last versions (all used SQL requests are really simple)
* Composer

## Setup
* Set **/public** folder as DocumentRoot and make sure that web-server redirects all requests to index.php, e.g. the following settings will work on Apache2:
    ```
    ServerName localhost
    DocumentRoot /var/www/html/abstract/public
    DirectoryIndex /index.php
    <Directory /var/www/html/abstract/public>
        AllowOverride None
        Order Allow,Deny
        Allow from All
        FallbackResource /index.php
    </Directory>
    ```
* Clone repo
* Run `composer install`
* Open [App/Config.php](App/Config.php) and enter your database configuration data.
* Run dump.sql in database console

## Run app
* http://localhost - start-up page
* use credentials from table user to login
