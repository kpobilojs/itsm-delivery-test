
function alertSuccessAndRedirect(message, url) {
    alertify.alert(message);
    $('.ajs-content').addClass('bg-success');
    window.setTimeout(function () {
        window.location.replace(url);
    }, 1500);
}